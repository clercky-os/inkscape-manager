/// Convenience macro to better work with the types at hand
#[macro_export]
macro_rules! press {
    ($d:expr, $key:expr, $mask:expr) => {
        use crate::manager;
        manager::press($d, $key as xlib::KeySym, $mask as u32);
    };
    ($d:expr, $key:expr) => {
        press!($d, $key, xlib::NoEventMask);
    };
}
