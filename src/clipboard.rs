use crate::{press, DisplayRef};
use anyhow::{Context, Result};
use std::{
    fs::{self, File},
    io::Write,
    time::Duration,
};
use x11::{keysym::XK_v, xlib};

use log::debug;
use subprocess::Popen;

const TARGET: &str = "image/x-inkscape-svg";

pub fn copy_to_target(d: &DisplayRef, content: &str, target: &str) -> Result<()> {
    debug!("Copying to target {target}: {content}");

    let mut proc = Popen::create(
        &["xclip", "-selection", "clipboard", "-i", "-target", target],
        subprocess::PopenConfig {
            stdin: subprocess::Redirection::Pipe,
            stdout: subprocess::Redirection::None,
            ..Default::default()
        },
    )
    .with_context(|| format!("Could not copy ({content}) to target ({target})"))?;
    proc.communicate(Some(content))?;

    // check for problems, only wait for max 3s
    let result = proc
        .wait_timeout(Duration::from_secs(3))
        .with_context(|| "xclip did not end in time")?;
    if result.is_none() {
        proc.terminate().unwrap();
    }

    press!(d, XK_v, xlib::ControlMask);

    Ok(())
}

pub fn write_txt_to_target(d: &DisplayRef, content: &str) -> Result<()> {
    let font_size = 10;
    let font = "monospace";
    let svg = format!("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
            <svg>
              <text
                 style=\"font-size:{font_size}px; font-family:'{font}';-inkscape-font-specification:'{font}, Normal';fill:#000000;fill-opacity:1;stroke:none;\"
                 xml:space=\"preserve\"><tspan sodipodi:role=\"line\" >{content}</tspan></text>
            </svg>");
    copy_to_target(d, svg.as_str(), TARGET)?;

    Ok(())
}

pub fn write_latex_to_target(d: &DisplayRef, content: &str) -> Result<()> {
    let latex = format!(
        "
        \\documentclass[12pt,border=12pt]{{standalone}}

        \\usepackage[utf8]{{inputenc}}
        \\usepackage[T1]{{fontenc}}
        \\usepackage{{textcomp}}
        \\usepackage{{amsmath, amssymb}}
        \\newcommand{{\\R}}{{\\mathbb R}}
        \\newcommand{{\\Z}}{{\\mathbb Z}}
        \\newcommand{{\\Q}}{{\\mathbb Q}}
        \\newcommand{{\\N}}{{\\mathbb N}}

        \\begin{{document}}
            {content}
        \\end{{document}}"
    );
    let master_tex_str = "master.tex";
    let master_svg_str = "master.svg";
    let master_pdf_str = "master.pdf";

    // compile latex
    let tempdir = tempfile::tempdir()?;
    let tempdir_path = tempdir.path();
    let master_tex_path = tempdir_path.join(master_tex_str);
    let mut master_tex = File::create(master_tex_path.clone())?;

    master_tex.write_all(latex.as_bytes())?;
    master_tex.flush()?;

    Popen::create(
        &["pdflatex", master_tex_path.to_str().unwrap()],
        subprocess::PopenConfig {
            cwd: Some(tempdir_path.as_os_str().to_os_string()),
            stdout: subprocess::Redirection::None,
            stderr: subprocess::Redirection::None,
            ..Default::default()
        },
    )?
    .wait()?;

    Popen::create(
        &["pdf2svg", master_pdf_str, master_svg_str],
        subprocess::PopenConfig {
            cwd: Some(tempdir_path.as_os_str().to_os_string()),
            stdout: subprocess::Redirection::None,
            stderr: subprocess::Redirection::None,
            ..Default::default()
        },
    )?
    .wait()?;

    let svg = fs::read_to_string(tempdir_path.join(master_svg_str))?;
    copy_to_target(d, svg.as_str(), TARGET)?;

    Ok(())
}
