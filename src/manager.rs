use std::os::raw::c_uint;

use log::debug;
use log::error;
use log::info;
use x11::keysym::*;
use x11::xlib;

use crate::xlib_bindings::*;

pub struct Manager {
    display: Display,
    root: xlib::Window,
    inkscape: xlib::Window,
    mode: Box<dyn ModeType>,
}

impl Manager {
    /// Constructor of Manager
    pub fn init(inkscape: xlib::Window, init_mode: Box<dyn ModeType>) -> Self {
        let display = Display::open().expect("Could not create connection to X");
        let screen_nr = display.get_default_screen();
        let root = display.get_root_window(screen_nr);

        Self {
            display,
            root,
            inkscape,
            mode: init_mode,
        }
    }

    /// Convenience to also start listening
    pub fn create(inkscape: xlib::Window, init_mode: Box<dyn ModeType>) {
        let m = Manager::init(inkscape, init_mode);
        m.listen();
    }

    pub fn listen(mut self) {
        self.grab();

        loop {
            let evt = self.display.next_evt();
            debug!("New evt detected");

            match evt.get_type() {
                xlib::KeyPress | xlib::KeyRelease => {
                    debug!("Key pressed");
                    let key_code = unsafe { evt.key.keycode };
                    let keysym = self.display.keycode_to_keysym(key_code as u8, 0);
                    let keymask = unsafe { evt.key.state };

                    // allow propagation
                    self.display
                        .allow_events(xlib::ReplayKeyboard, xlib::CurrentTime);

                    // run current mode and set mode for next round
                    self.mode = self.mode.run(
                        DisplayRef {
                            display: &self.display,
                            root: &self.root,
                            inkscape: &self.inkscape,
                        },
                        KeyEvent {
                            event: evt.to_owned(),
                            keysym,
                            keymask,
                        },
                    );
                }
                xlib::DestroyNotify => {
                    if unsafe { evt.destroy_window.event } == self.inkscape {
                        // clean up
                        info!("Cleaning up on destroy");
                        self.ungrab();
                        return;
                    }
                }
                xlib::ButtonPress => {
                    debug!("Button pressed");
                }
                xlib::ButtonRelease => {
                    debug!("Button released");
                }
                _ => {}
            };
        }
    }

    pub fn grab(&self) {
        // setup listeners
        self.display.select_input(
            &self.inkscape,
            xlib::KeyReleaseMask | xlib::KeyPressMask | xlib::SubstructureNotifyMask,
        );

        // grab all keys
        self.display.grab_key(
            &self.inkscape,
            xlib::AnyKey,
            xlib::AnyModifier,
            true,
            xlib::GrabModeAsync,
            xlib::GrabModeAsync,
        );

        // Ungrab all window manager keys
        self.display.ungrab_key(
            &self.inkscape,
            self.display.keysym_to_keycode(XK_Super_L as u64) as i32,
            xlib::AnyModifier,
        );
        self.display.ungrab_key(
            &self.inkscape,
            self.display.keysym_to_keycode(XK_Alt_L as u64) as i32,
            xlib::AnyModifier,
        );
    }

    pub fn ungrab(&self) {
        self.display
            .ungrab_key(&self.inkscape, xlib::AnyKey, xlib::AnyModifier);
    }
}

pub fn press(d: &DisplayRef, key: xlib::KeySym, mask: c_uint) {
    let keycode = d.display.keysym_to_keycode(key);

    // Key press evt
    let mut key_press: xlib::XEvent = unsafe { std::mem::zeroed() };
    key_press.key.type_ = xlib::KeyPress;
    key_press.key.time = xlib::CurrentTime;
    key_press.key.display = d.display.0;
    key_press.key.root = *d.root;
    key_press.key.window = *d.inkscape;
    key_press.key.state = mask.to_owned() as u32;
    key_press.key.keycode = keycode as u32;

    // Key release evt
    let mut key_release: xlib::XEvent = unsafe { std::mem::zeroed() };
    key_release.key.type_ = xlib::KeyRelease;
    key_release.key.time = xlib::CurrentTime;
    key_release.key.display = d.display.0;
    key_release.key.root = *d.root;
    key_release.key.window = *d.inkscape;
    key_release.key.state = mask.to_owned() as u32;
    key_release.key.keycode = keycode as u32;

    // send events
    // The 0 is important otherwise the event is not received by inkscape!
    // If mask is empty set (=0), then the event is send to the window. propagate
    // needs to be true in this case!
    debug!("Sending button press to inkscape");
    if d.display
        .send_event(d.inkscape, true, 0, &mut key_press)
        .is_err()
    {
        error!("Could not send press event");
    }
    if d.display
        .send_event(d.inkscape, true, 0, &mut key_release)
        .is_err()
    {
        error!("Could not send release event");
    }

    d.display.flush();
    d.display.sync(true);
}

pub struct KeyEvent {
    pub event: xlib::XEvent,  // The original event
    pub keysym: xlib::KeySym, // The retrieved keysym
    pub keymask: c_uint,      // OR of modifiers like ShiftMask|LockMask|ControlMask|...
}

impl KeyEvent {
    /// Filter only the control and shift keys
    pub fn filter_boring_mods(&self) -> c_uint {
        self.keymask & (xlib::ControlMask | xlib::ShiftMask)
    }

    /// true if the keysym is a modifier
    #[allow(non_upper_case_globals)]
    pub fn is_mod(&self) -> bool {
        matches!(
            self.keysym as c_uint,
            XK_Shift_L | XK_Shift_R | XK_Control_L | XK_Control_R
        )
    }
}

pub trait ModeType {
    fn run(self: Box<Self>, d: DisplayRef, key: KeyEvent) -> Box<dyn ModeType>;
}
