use std::thread;

use log::{debug, info};
use x11::xlib;

pub mod clipboard;
pub mod manager;
pub mod normal;
pub mod utils;
pub mod vim;
pub mod xlib_bindings;

use xlib_bindings::*;

use crate::{manager::Manager, normal::NormalMode};

fn main() {
    pretty_env_logger::init();

    setup();
}

// setup of X and listeners
fn setup() -> ! {
    let display = Display::open().expect("Could not open display");
    let screen = display.get_default_screen();
    let root = display.get_root_window(screen);

    // search for existing windows
    let tree = display.query_tree(&root).expect("Could not query root");
    for window in tree.children {
        if is_inkscape(&display, window) {
            info!("Found existing inkscape window");
            let window = window.to_owned();
            thread::spawn(move || {
                let mode: NormalMode = Default::default();
                Manager::create(window, Box::new(mode));
            });
        }
    }

    // wait for new windows
    display.select_input(&root, xlib::SubstructureNotifyMask); // set event listener
    loop {
        // wait eternaly for new windows
        let evt = display.next_evt();
        unsafe {
            if let xlib::CreateNotify = evt.type_ {
                let window = evt.create_window.window;
                if is_inkscape(&display, &window) {
                    info!("New inkscape window");
                    let window = window.to_owned();
                    thread::spawn(move || {
                        let mode: NormalMode = Default::default();
                        Manager::create(window, Box::new(mode));
                    });
                }
            }
        }
    }
}

fn is_inkscape(display: &Display, window: &xlib::Window) -> bool {
    let class_hint = display.get_class_hint(window);
    if let Ok(class_hint) = class_hint {
        let class = class_hint.get_class();
        debug!("Detected window with classname: {}", class);
        class.contains("Inkscape")
    } else {
        false
    }
}
