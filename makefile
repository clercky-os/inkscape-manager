build:
	cargo build

run:
	cargo run

lint:
	cargo watch -x clippy
