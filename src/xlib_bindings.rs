use std::{
    ffi::CStr,
    mem::MaybeUninit,
    os::raw::{c_int, c_long, c_uint, c_ulong},
    ptr,
};

use log::warn;
use x11::xlib;

extern "C" fn xerror(_d: *mut xlib::Display, err: *mut xlib::XErrorEvent) -> i32 {
    warn!(
        "fatal error: request code={}, error code={}",
        unsafe { (*err).request_code },
        unsafe { (*err).error_code }
    );

    0
}

#[derive(Debug)]
pub struct XStatusError();

pub struct Display(pub *mut xlib::Display);

impl Display {
    /// Opens a connection with X
    pub fn open() -> Option<Self> {
        unsafe {
            // set default handler
            xlib::XSetErrorHandler(Some(xerror));

            let display = xlib::XOpenDisplay(ptr::null());
            if display.is_null() {
                None
            } else {
                Some(Display(display))
            }
        }
    }

    /// Gives back the main screen number (only single screen env)
    pub fn get_default_screen(&self) -> c_int {
        unsafe { xlib::XDefaultScreen(self.0) }
    }

    pub fn get_root_window(&self, screen_number: c_int) -> xlib::Window {
        unsafe { xlib::XRootWindow(self.0, screen_number) }
    }

    pub fn query_tree(&self, window: &xlib::Window) -> Result<QueryTreeRestult, XStatusError> {
        unsafe {
            let mut root = MaybeUninit::uninit();
            let mut parent = MaybeUninit::uninit();
            let mut children = MaybeUninit::uninit();
            let mut nchildren = MaybeUninit::uninit();

            let status = xlib::XQueryTree(
                self.0,
                *window,
                root.as_mut_ptr(),
                parent.as_mut_ptr(),
                children.as_mut_ptr(),
                nchildren.as_mut_ptr(),
            );

            if status == 0 {
                Err(XStatusError())
            } else {
                Ok(QueryTreeRestult {
                    root: root.assume_init(),
                    parent: parent.assume_init(),
                    children: std::slice::from_raw_parts(
                        children.assume_init(),
                        nchildren.assume_init() as usize,
                    ),
                })
            }
        }
    }

    pub fn select_input(&self, window: &xlib::Window, evt_mask: c_long) {
        unsafe {
            xlib::XSelectInput(self.0, *window, evt_mask);
        }
    }

    pub fn change_window_attr(
        &self,
        window: &xlib::Window,
        value_mask: c_ulong,
        attr: &mut xlib::XSetWindowAttributes,
    ) {
        unsafe {
            xlib::XChangeWindowAttributes(
                self.0,
                *window,
                value_mask,
                attr as *mut xlib::XSetWindowAttributes,
            );
        }
    }

    /// TODO: Get the wm_name the proper way (XTextProperty)
    pub fn get_wm_name(&self, window: &xlib::Window) -> Result<String, XStatusError> {
        unsafe {
            let mut text_prop = MaybeUninit::uninit();
            let status = xlib::XGetWMName(self.0, *window, text_prop.as_mut_ptr());

            if status == 0 {
                Err(XStatusError())
            } else {
                Ok(CStr::from_ptr(text_prop.assume_init().value.cast())
                    .to_str()
                    .unwrap()
                    .to_owned())
            }
        }
    }

    pub fn get_class_hint(&self, window: &xlib::Window) -> Result<xlib::XClassHint, XStatusError> {
        unsafe {
            let mut class_hint = MaybeUninit::uninit();
            let status = xlib::XGetClassHint(self.0, *window, class_hint.as_mut_ptr());

            if status == 0 {
                Err(XStatusError {})
            } else {
                Ok(class_hint.assume_init())
            }
        }
    }

    /// Get next event and blocks when queue is empty
    pub fn next_evt(&self) -> xlib::XEvent {
        unsafe {
            let mut evt = MaybeUninit::uninit();
            xlib::XNextEvent(self.0, evt.as_mut_ptr());
            evt.assume_init()
        }
    }

    /// @param pointer_mode GrabMode(A)Sync
    /// @param keyboard_mode GrabMode(A)Sync
    pub fn grab_key(
        &self,
        window: &xlib::Window,
        key_code: c_int,
        mods: c_uint,
        owner_evts: bool,
        pointer_mode: c_int,
        keyboard_mode: c_int,
    ) {
        unsafe {
            xlib::XGrabKey(
                self.0,
                key_code,
                mods,
                *window,
                bool2xlib(owner_evts),
                pointer_mode,
                keyboard_mode,
            );
        }
    }

    pub fn ungrab_key(&self, window: &xlib::Window, key_code: c_int, mods: c_uint) {
        unsafe {
            xlib::XUngrabKey(self.0, key_code, mods, *window);
        }
    }

    pub fn keysym_to_keycode(&self, keysym: xlib::KeySym) -> xlib::KeyCode {
        unsafe { xlib::XKeysymToKeycode(self.0, keysym) }
    }

    pub fn keycode_to_keysym(&self, keycode: xlib::KeyCode, index: c_int) -> xlib::KeySym {
        unsafe { xlib::XKeycodeToKeysym(self.0, keycode, index) }
    }

    /// Allow further events to be processed when the device has been frozen
    /// @param event_mode needs enum xlib::{AsyncPointer, SyncPointer, AsyncKeyboard, SyncKeyboard,
    /// ReplayPointer, ReplayKeyBoard, AsyncBoth, SyncBoth}
    pub fn allow_events(&self, event_mode: c_int, time: xlib::Time) {
        unsafe {
            xlib::XAllowEvents(self.0, event_mode, time);
        }
    }

    /// When sending a keyboard event to a specific window, send it with propagate true
    /// and event_mask 0 (empty). Otherwise the destination you try to get to may
    /// be filtered out.
    pub fn send_event(
        &self,
        window: &xlib::Window,
        propagate: bool,
        event_mask: c_long,
        event_send: &mut xlib::XEvent,
    ) -> Result<(), XStatusError> {
        unsafe {
            let status = xlib::XSendEvent(
                self.0,
                *window,
                bool2xlib(propagate),
                event_mask,
                event_send as *mut xlib::XEvent,
            );

            if status == 0 {
                Err(XStatusError())
            } else {
                Ok(())
            }
        }
    }

    pub fn flush(&self) {
        unsafe {
            xlib::XFlush(self.0);
        }
    }
    pub fn sync(&self, discard: bool) {
        unsafe {
            xlib::XSync(self.0, bool2xlib(discard));
        }
    }
}

impl Drop for Display {
    fn drop(&mut self) {
        unsafe { xlib::XCloseDisplay(self.0) };
    }
}

pub struct QueryTreeRestult<'a> {
    pub parent: xlib::Window,
    pub root: xlib::Window,
    pub children: &'a [xlib::Window],
}

pub trait ClassHint {
    fn get_name(&self) -> &str;
    fn get_class(&self) -> &str;
}

impl ClassHint for xlib::XClassHint {
    fn get_name(&self) -> &str {
        unsafe { CStr::from_ptr(self.res_name).to_str() }.expect("Could not retrieve name")
    }

    fn get_class(&self) -> &str {
        unsafe { CStr::from_ptr(self.res_class).to_str() }.expect("Could not retrieve class")
    }
}

/// Reference to display properties
pub struct DisplayRef<'a> {
    pub display: &'a Display,
    pub root: &'a xlib::Window,
    pub inkscape: &'a xlib::Window,
}

/// Transform rust's bool to a bool for X
pub const fn bool2xlib(b: bool) -> i32 {
    if b {
        xlib::True
    } else {
        xlib::False
    }
}
