use std::{
    fs::{remove_file, File},
    io::{Read, Write},
    path::Path,
};

use log::error;
use subprocess::{Popen, PopenError};

use crate::{clipboard, xlib_bindings::DisplayRef};

pub fn open_vim(d: &DisplayRef, compile_latex: bool) {
    let latex_filename = {
        // setup tempfile with initial input
        let latex_file = if let Ok(res) = tempfile::Builder::new().suffix(".tex").tempfile() {
            res
        } else {
            log::error!("Couldn't create tempfile");
            return;
        };
        let mut latex_file = if let Ok(res) = latex_file.keep() {
            res
        } else {
            log::error!("Couldn't persist tempfile");
            return;
        };

        if latex_file.0.write_all(b"$$").is_err() {
            log::error!("Couldn't write to tempfile");
            return;
        }

        // remeber path end close file
        latex_file.1
    };

    // feed it to vim
    if open_editor(&latex_filename).is_err() {
        log::error!("Couldn't start editor");
        return;
    }

    let latex: String = {
        let mut file = File::open(&latex_filename).expect("We just created this, NOT POSSIBLE");
        let mut buf = Vec::with_capacity(250);
        if file.read_to_end(&mut buf).is_err() {
            log::error!("Couldn't read contents of tempfile");
            return;
        }
        String::from_utf8_lossy(&buf).to_string()
    };

    // clean up after use
    if remove_file(latex_filename).is_err() {
        log::error!("Couldn't remove tempfile");
        return;
    }

    let clipboard_result = if compile_latex {
        clipboard::write_latex_to_target(d, latex.as_str())
    } else {
        clipboard::write_txt_to_target(d, latex.as_str())
    };
    if let Err(err) = clipboard_result {
        error!("Could not paste to target (compiling? {compile_latex}): {err}");
    }
}

fn open_editor(file_path: &Path) -> Result<(), PopenError> {
    Popen::create(
        &[
            "alacritty",
            "-e",
            "vim",
            file_path.to_str().expect("Invalid file system"),
        ],
        Default::default(),
    )?;

    Ok(())
}
