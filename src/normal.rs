use std::collections::HashSet;
use std::os::raw::c_uint;

use crate::manager::*;
use crate::{vim, xlib_bindings::*};
use log::error;
use log::{debug, trace};
use x11::keysym::*;
use x11::xlib;

#[derive(Default)]
pub struct NormalMode {
    events: Vec<xlib::XEvent>,
    pressed_keys: HashSet<(xlib::KeySym, c_uint)>, // keysym + keymask
}

impl ModeType for NormalMode {
    fn run(mut self: Box<Self>, d: DisplayRef, evt: KeyEvent) -> Box<dyn ModeType> {
        self.events.push(evt.event.to_owned());

        match evt.event.get_type() {
            xlib::KeyPress => {
                if !evt.is_mod() {
                    // check if not shift or control
                    self.pressed_keys
                        .insert((evt.keysym, evt.filter_boring_mods()));
                }
                self
            } // dont do anything and reschedule
            xlib::KeyRelease => {
                let (handled, maybe_mode) = match self.pressed_keys.len() {
                    1 => {
                        let (keysym, mask) = self
                            .pressed_keys
                            .iter()
                            .next()
                            .expect("Impossible, iterator should have a first element");
                        self.handle_single_key(&d, keysym, mask)
                    }
                    _ => {
                        self.paste_style();
                        (true, None)
                    }
                };

                // replay event to inkscape if not handled
                if !handled {
                    self.replay(d.display, d.inkscape)
                }

                self.events.clear();
                self.pressed_keys.clear();

                if let Some(new_mode) = maybe_mode {
                    new_mode
                } else {
                    self
                }
            }
            _ => {
                error!("Impossible!!!");
                self
            }
        }
    }
}

impl NormalMode {
    fn replay(&self, d: &Display, inkscape: &xlib::Window) {
        self.events.iter().for_each(|evt| {
            if d.send_event(inkscape, true, xlib::NoEventMask, &mut evt.to_owned())
                .is_err()
            {
                error!("Could not replay events");
            }
        });
        d.flush();
        d.sync(true);
    }

    fn paste_style(&self) {}

    /// @returns (is_handled, optional next state)
    fn handle_single_key(
        &self,
        d: &DisplayRef,
        keysym: &xlib::KeySym,
        keymask: &c_uint,
    ) -> (bool, Option<Box<dyn ModeType>>) {
        debug!("Handling singe key");
        let keysym = keysym.to_owned() as u32;
        let keymask = keymask.to_owned() as u32;

        // remap keys + change state if needed
        // return format: (handled, next_mode?)
        use crate::press;
        #[allow(non_upper_case_globals)]
        match (keysym, keymask) {
            (XK_y, xlib::ShiftMask) => {
                debug!("Opening vim + no compile");
                vim::open_vim(d, false);
                (true, None)
            }
            (XK_y, _) => {
                debug!("Opening vim + compile");
                vim::open_vim(d, true);
                (true, None)
            }
            (XK_a, xlib::ShiftMask) => {
                trace!("<S-a> -> <S-a>");
                press!(d, XK_a, xlib::ShiftMask);
                (true, None)
            }
            (XK_a, _) => {
                trace!("a -> s");
                press!(d, XK_s);
                (true, None)
            }
            (XK_s, xlib::ShiftMask) => {
                trace!("<S-s> -> <S-s>");
                press!(d, XK_s, xlib::ShiftMask);
                (true, None)
            }
            (XK_s, _) => {
                trace!("s -> s");
                press!(d, XK_s);
                (true, None)
            }
            (XK_p, _) => {
                trace!("p -> p");
                press!(d, XK_p);
                (true, None)
            } // pencil
            (XK_j, _) => {
                trace!("j -> <S-%>");
                press!(d, XK_percent, xlib::ShiftMask);
                (true, None)
            } // snap toggle
            (XK_apostrophe, xlib::ShiftMask) => {
                trace!("' -> DEL");
                press!(d, XK_Delete);
                (true, None)
            } // delete
            (XK_apostrophe, _) => {
                trace!("' -> <C-z>");
                press!(d, XK_z, xlib::ControlMask);
                (true, None)
            } // undo
            (XK_dollar, _) => {
                trace!("$ -> `");
                press!(d, XK_grave);
                (true, None)
            } // backtick
            (XK_u, xlib::ControlMask) => {
                trace!("<C-u> -> <C-d>");
                press!(d, XK_d, xlib::ControlMask);
                (true, None)
            }
            _ => (false, None), // evt not handled
        }
    }
}
